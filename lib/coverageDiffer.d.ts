import { IJsonSummary } from './common';
export declare const coverageDiffer: (base: IJsonSummary, head: IJsonSummary) => IJsonSummary;
