import { IFilesResults, IFileResultFormat } from './common';
export declare const resultFormatter: (files: IFilesResults, total: IFileResultFormat) => string;
